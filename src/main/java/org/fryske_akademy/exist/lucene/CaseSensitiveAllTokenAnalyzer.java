package org.fryske_akademy.exist.lucene;

/*-
 * #%L
 * exist-db-addons
 * %%
 * Copyright (C) 2020 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.util.Version;

import java.io.Reader;

/**
 * all tokens are token characters, tokens are not converted
 */
public class CaseSensitiveAllTokenAnalyzer extends Analyzer {
    public CaseSensitiveAllTokenAnalyzer(Version version) {
        this();
    }

    public CaseSensitiveAllTokenAnalyzer() {
    }

    @Override
    protected TokenStreamComponents createComponents(String arg0, Reader arg1) {
        Tokenizer tokenizer = new NoSeparatorTokenizer(arg1);
        return new TokenStreamComponents(tokenizer);
    }
}
