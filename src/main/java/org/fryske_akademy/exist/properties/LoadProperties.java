package org.fryske_akademy.exist.properties;

/*-
 * #%L
 * exist-db-addons
 * %%
 * Copyright (C) 2020 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.exist.dom.QName;
import org.exist.xquery.BasicFunction;
import org.exist.xquery.Cardinality;
import org.exist.xquery.ErrorCodes;
import org.exist.xquery.FunctionSignature;
import org.exist.xquery.Module;
import org.exist.xquery.XPathException;
import org.exist.xquery.XQueryContext;
import org.exist.xquery.functions.map.MapType;
import org.exist.xquery.value.FunctionParameterSequenceType;
import org.exist.xquery.value.FunctionReturnSequenceType;
import org.exist.xquery.value.Sequence;
import org.exist.xquery.value.SequenceType;
import org.exist.xquery.value.StringValue;
import org.exist.xquery.value.Type;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Function to load properties from a file under a basePath into a map
 */
public class LoadProperties extends BasicFunction {

    private final static Logger logger = LogManager.getLogger(LoadProperties.class);

    public final static FunctionSignature signature =
            new FunctionSignature(
                    new QName("loadProperties", PropertiesModule.NAMESPACE_URI, PropertiesModule.PREFIX),
                    "A function to load properties from a file under a basePath into a map.",
                    new SequenceType[]{new FunctionParameterSequenceType("filename", Type.STRING, Cardinality.EXACTLY_ONE, "The filename (without directories) of the properties to load")},
                    new FunctionReturnSequenceType(Type.MAP, Cardinality.EXACTLY_ONE, "the loaded properties or an empty map"));

    public LoadProperties(XQueryContext context) {
        super(context, signature);
    }

    public MapType eval(Sequence[] args, Sequence contextSequence)
            throws XPathException {
        MapType properties = new MapType(getContext());
        if (!args[0].isEmpty()) {
            String name = args[0].getStringValue();
            if (name.contains(File.separator)) {
                throw new IllegalArgumentException("Supply only the name of the properties file");
            }
            Module[] modules = getContext().getModules(PropertiesModule.NAMESPACE_URI);
            if (modules==null || modules.length!=1) {
                throw new IllegalArgumentException(String.format("%s module not found",PropertiesModule.NAMESPACE_URI));
            }
            PropertiesModule pm = (PropertiesModule) modules[0];
            String basePath = pm.getBasePath();
            String sep = basePath.endsWith(File.separator) ? "" : File.separator;
            File p = new File(pm.getBasePath()+sep+name);
            if (p.isFile() && p.canRead()) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("loading properties from %s", p.getPath()));
                }
                Properties load = new Properties();
                try {
                    load.load(new FileReader(p));
                    load.forEach((k, v) ->
                            properties.add(new StringValue(String.valueOf(k)),
                                    new StringValue(String.valueOf(v))
                            )
                    );
                } catch (IOException e) {
                    throw new XPathException(ErrorCodes.FODC0002,
                            String.format("Unable to read properties from %s", p.getPath()),
                            e
                    );
                }
            } else {
                throw new XPathException(ErrorCodes.FODC0002,
                        String.format("Unable to read properties from %s", p.getPath())
                );
            }
        }
        return properties;
    }

}
