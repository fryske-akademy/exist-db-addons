package org.fryske_akademy.exist.properties;

/*-
 * #%L
 * exist-db-addons
 * %%
 * Copyright (C) 2020 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.exist.xquery.AbstractInternalModule;
import org.exist.xquery.FunctionDef;

import java.io.File;
import java.util.List;
import java.util.Map;

public class PropertiesModule extends AbstractInternalModule {

    public final static String NAMESPACE_URI = "http://exist-db.org/xquery/properties";

    public final static String PREFIX = "properties";
    public final static String INCLUSION_DATE = "2020-07-20";
    public final static String RELEASED_IN_VERSION = "eXist-5.2.0";

    private final static FunctionDef[] functions = {
            new FunctionDef(LoadProperties.signature, LoadProperties.class)
    };
    public static final String BASE_PATH = "basePath";

    public PropertiesModule(Map<String, List<?>> parameters) {
        super(functions, parameters);
    }

    public String getNamespaceURI() {
        return NAMESPACE_URI;
    }

    public String getDefaultPrefix() {
        return PREFIX;
    }

    public String getDescription() {
        return "A module for reading properties into a map, since " + INCLUSION_DATE;
    }

    public String getReleaseVersion() {
        return RELEASED_IN_VERSION;
    }

    public String getBasePath() {
        List<String> basePath = (List<String>) getParameter(BASE_PATH);
        if (basePath.isEmpty()) {
            throw new IllegalStateException("Parameter required: " + BASE_PATH);
        }
        File path = new File(basePath.get(0));
        if (path.isDirectory()&&path.isAbsolute()&&path.canRead()) {
            return basePath.get(0);
        } else {
            throw new IllegalStateException("Not an absolute path or cannot be read: " + path.getPath());
        }
    }

}
