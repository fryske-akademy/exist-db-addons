/*-
 * #%L
 * exist-db-addons
 * %%
 * Copyright (C) 2020 - 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import org.exist.xmldb.XmldbURI;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSync {
    @Test
    public void testCompare() throws UnsupportedEncodingException, URISyntaxException {
        Path resources = new File("src/test/resources").toPath();
        XmldbURI u = XmldbURI.xmldbUriFor("hûs.xml");
        String decoded = URLDecoder.decode(u.toString(),"utf-8");
        Assertions.assertTrue(Files.exists(resources.resolve(decoded)));
    }
}
