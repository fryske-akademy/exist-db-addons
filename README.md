# README #

This project contains add-ons for exist-db.

Available in maven central: https://search.maven.org/search?q=g:org.fryske-akademy

## cronjobs for importing data

These jobs enable importing data from filesystem recursively, for example from a docker volume. Only new or newer files will be imported,
files not present in the source will be deleted from the target collection by default.

### usage

- include jar in classpath, for example in docker:
```dockerfile
ARG EXISTADDONSERSION=2.6
COPY exist-db-addons-${EXISTADDONSERSION}.jar $EXIST_HOME/lib/
ENV CLASSPATH=$EXIST_HOME/lib/exist.uber.jar:$EXIST_HOME/lib/exist-db-addons-${EXISTADDONSERSION}.jar
```
- configure syn task in conf.xml (first job is startup, second cron scheduled):
```xml
    <job class="org.fryske_akademy.exist.jobs.DataSyncTask" type="system" period="10" repeat="0" >
        <parameter name="collection" value="xmldb:exist:///db/apps/teidictjson/data"/>
        <parameter name="datadir" value="/data"/>
    </job>
    <job class="org.fryske_akademy.exist.jobs.DataSyncTaskCron" type="system" cron-trigger="0 0 2 ? * *" >
        <parameter name="collection" value="xmldb:exist:///db/apps/teidictjson/data"/>
        <parameter name="datadir" value="/data"/>
    </job>
```

## property file loader

A function loadproperties(filename) to read property files below a configured basePath in
the http://exist-db.org/xquery/properties namespace.

### usage

- include jar in classpath, for example in docker:
```dockerfile
ARG EXISTADDONSERSION=2.6
COPY exist-db-addons-${EXISTADDONSERSION}.jar $EXIST_HOME/lib/
ENV CLASSPATH=$EXIST_HOME/lib/exist.uber.jar:$EXIST_HOME/lib/exist-db-addons-${EXISTADDONSERSION}.jar
```
- configure module in conf.xml:
```xml
    <module uri="http://exist-db.org/xquery/properties"
            class="org.fryske_akademy.exist.properties.PropertiesModule">
        <parameter name="basePath" value="/run/secrets"/>
    </module>
```
- optionally mount properties to exist-db as docker secrets:
```dockerfile
    secrets:
      - source: ${APPNAME}.properties
        target: ${APPNAME}.properties
        mode: 0444
secrets:
  teidictjson.properties:
    external: true

```
- use properties in your xquery:
```xquery

declare namespace properties="http://exist-db.org/xquery/properties";

declare variable $teidictjson:props := properties:loadProperties("teidictjson.properties");

declare function teidictjson:getProperty($key as xs:string, $default as xs:string) as xs:string {
    if (map:contains($teidictjson:props,$key)) then
        map:get($teidictjson:props,$key)
    else
        $default
};
```
## lucene analyzers

Some case insensitive lucene anayzers that may be of use, one considers ";" or "; " to be separators.